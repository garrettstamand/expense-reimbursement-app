# Expense Reimbursement App

## Project Description
The Expense Reimbursement App is an application that allows employees to make reimbursement requests for various business expenses, such as travel, lodging and food. Managers can log in through the portal and either deny or allow individual requests, as well as sort and filter the requests.

## Technologies Used
- Maven
- Java
- PostgreSQL
- Javascript
- HTML
- CSS

## Features
- Persists changes to a database
- Uses Jasypt to encrypt user passwords
- Includes the functionality for managers to make requests

## Getting Started
1. Using GitBash, use the 'cd' command to change directories until you are where you want the project to download to.
2. Type in 'git clone https://gitlab.com/garrettstamand/expense-reimbursement-app.git'
3. Install JDK 8 (https://www.oracle.com/java/technologies/downloads/) and Apache Tomcat (https://tomcat.apache.org/download-80.cgi) on your computer.
## Usage
1. Open a Java IDE of your choice (I prefer Intellij) and add Smart Tomcat as a plugin. Configure it by pointing to where you installed Tomcat and leave the context path blank.
2. Ensure you set the environment variables for "AWS_URL", "AWS_USERNAME", and "AWS_PASSWORD" to your own database credentials.
3. Run the program and then open a browser to http://localhost:port_number, where port_number is whatever you chose when you configured Tomcat.
