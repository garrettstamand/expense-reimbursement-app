
window.onload = async()=>{
    let loginForm = document.getElementById("login-form");
    console.log(loginForm);

    loginForm.onsubmit = async function(e){
        e.preventDefault();

        //get values from the input field
        let username = document.getElementById("username").value;
        let password = document.getElementById("password").value;

        //how do we send values to the backend?
        let response = await fetch(`/ERS/api/AttemptLogin`,{
            method: "POST",
            body: JSON.stringify({
                username: username,
                password: password
            })
        })

        let responseData = await response.json();

        if(responseData.success){
            if(responseData.data.roleID == 1) window.location = `manager-dashboard.html`
            if(responseData.data.roleID == 2) window.location = `employee-dashboard.html`
        }else{
            let messageElem = document.getElementById("login-message")
            messageElem.innerText = responseData.message
        }
    


    }
}