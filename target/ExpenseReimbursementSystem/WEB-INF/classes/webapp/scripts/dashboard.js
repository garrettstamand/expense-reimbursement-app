window.onload = async()=>{

    checkLogin();

    getReimbursements();
    var filter = document.getElementById("filterByDecision");
    var sort = document.getElementById("sortByDecision");
    var submitRequest = document.getElementById("submitRequestButton");
    var logout = document.getElementById("logout");

    if(submitRequest != null){
        submitRequest.onclick =()=>{
            submitNewRequest();
        }
    }

    filter.onchange =()=>{ getReimbursements();}
    sort.onchange =()=>{ getReimbursements();}

    logout.onclick = async()=>{
        let response = await fetch("/ERS/api/Logout");
        window.location = "index.html";
    }
}

async function getReimbursements(){

    let filter = document.getElementById("filterByDecision");
    var sort = document.getElementById("sortByDecision");

    let reimbursementTBody = document.getElementById("reimbursementTBody");
    let reimbursementTHead = document.getElementById("reimbursementTHead");
    reimbursementTBody.innerHTML = "";
    reimbursementTHead.innerHTML = "";

    const checkSessionResponse = await fetch("/ERS/api/CheckSession");
    const sessionData = await checkSessionResponse.json();
    let manager = sessionData.success;

    if(window.location.pathname == "/ERS/employee-dashboard.html") manager = false;

    var response;
    if(manager) response = await fetch("/ERS/api/GetAllReimbursementRequests");
    else response = await fetch("/ERS/api/ViewPastTickets");
    
    const data = await response.json();

    if(sort.value == "idAscending") data.sort((a, b)=>{return a.id - b.id;});
    else if(sort.value == "idDescending") data.sort((a, b)=>{return b.id - a.id;});
    else if(sort.value == "amountAscending") data.sort((a, b)=>{return a.amount - b.amount;});
    else if(sort.value == "amountDescending") data.sort((a, b)=>{return b.amount - a.amount;});
    else if(sort.value == "dateNewest") data.sort((a, b)=>{return b.submitted - a.submitted});
    else if(sort.value == "dateOldest") data.sort((a, b)=>{return a.submitted - b.submitted});

    data.forEach(reimbursementRequest => {

        if(filter.value != "All"){
            if(filter.value != reimbursementRequest.status) return;
        }

        let submittedDate = new Date(reimbursementRequest.submitted);
        let resolvedDate;
        if(reimbursementRequest.resolved == null) resolvedDate = "Not resolved";
        else resolvedDate = new Date(reimbursementRequest.resolved).toLocaleDateString() + " " + new Date(reimbursementRequest.resolved).toLocaleTimeString();
        let resolver = reimbursementRequest.resolver;

        if(resolver == "null null") resolver = "Not resolved";

        let reimbursement = document.createElement(`tr`);
        reimbursement.className = "table-tr"
        reimbursement.innerHTML = `
        <td class="table-td">${reimbursementRequest.id}</td>
        <td class="table-td">$${reimbursementRequest.amount}</td>
        <td class="table-td">${reimbursementRequest.type}</td>
        <td class="table-td">${reimbursementRequest.description}</td>
        <td class="table-td">${submittedDate.toLocaleDateString()} ${submittedDate.toLocaleTimeString()}</td>
        <td class="table-td">${resolvedDate}</td>
        <td class="table-td">${reimbursementRequest.author}</td>
        <td class="table-td">${resolver}</td>
        `;

        if(manager && reimbursementRequest.status == "Pending") reimbursement.innerHTML += `<td class="table-td"><button id="approveButton" onClick="approveReimbursement(${reimbursementRequest.id})">Approve</button> <button id="denyButton" onClick="denyReimbursement(${reimbursementRequest.id})">Deny</button></td>`;
        else reimbursement.innerHTML += `<td class="table-td">${reimbursementRequest.status}</td>`;

        reimbursementTBody.appendChild(reimbursement);
        reimbursementTHead.innerHTML = `<tr class="table-tr">
        <th>ID</th>
        <th>Amount</th>
        <th>Type</th>
        <th>Description</th>
        <th>Date Submitted</th>
        <th>Date Resolved</th>
        <th>Author</th>
        <th>Resolver</th>
        <th>Decision</th>
        </tr>`;
    });
}

async function submitNewRequest(){

    let reimbursementAmount = document.getElementById("reimbursementAmount").value;
    let reimbursementType = document.getElementById("reimbursementType").value;
    let reimbursementDescription = document.getElementById("reimbursementDescription").value;

    console.log(reimbursementAmount + " " + reimbursementDescription + " " + reimbursementType);
    
    let response = await fetch(`/ERS/api/AddReimbursementRequest`,{
        method: "POST",
        body: JSON.stringify({
            id:0,
            amount:reimbursementAmount,
            submitted:null,
            resolved:null,
            description:reimbursementDescription,
            receipt:null,
            author:0,
            resolver:0,
            status:0,
            type:reimbursementType
        })
    })

    getReimbursements();
}

async function approveReimbursement(id){

    let createRes = await fetch(`/ERS/api/ApproveReimbursement`,{
        method: "PUT",
        body: JSON.stringify({
            reimbursementID: id,
            approved: true
        })
    })
    getReimbursements()
}
async function denyReimbursement(id){
    let createRes = await fetch(`/ERS/api/ApproveReimbursement`,{
        method: "PUT",
        body: JSON.stringify({
            reimbursementID: id,
            approved: false
        })
    })
    getReimbursements()
}
async function checkLogin(){
    fetch("/ERS/api/CheckSession")
    .then(x => x.json())
    .then(sessionData =>{
        let user = sessionData.data;
        let manager = sessionData.success;

        if(user == null) window.location = "index.html";
        if(!manager && window.location.pathname == "/ERS/manager-dashboard.html") window.location = "index.html";
        if(manager){
            let gotoEmployeeDashboard = document.getElementById("gotoEmployeeDashboard");
            if(gotoEmployeeDashboard != null) gotoEmployeeDashboard.innerHTML = `<a href="employee-dashboard.html">Go to Employee Dashboard</a>`;
            let gotoManagerDashboard = document.getElementById("gotoManagerDashboard");
            if(gotoManagerDashboard != null) gotoManagerDashboard.innerHTML = `<a href="manager-dashboard.html">Go to Manager Dashboard</a>`;
        }
    });

}