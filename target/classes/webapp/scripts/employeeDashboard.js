window.onload = async()=>{
    getReimbursements();
}

async function getReimbursements(){

    let filter = document.getElementById("filterByDecision");

    let reimbursementTBody = document.getElementById("reimbursementTBody");
    let reimbursementTHead = document.getElementById("reimbursementTHead");

    reimbursementTBody.innerHTML = "";
    reimbursementTHead.innerHTML = "";

    const response = await fetch("/ERS/api/ViewPastTickets");
    const data = await response.json();

    data.forEach(reimbursementRequest => {


        let date = new Date(reimbursementRequest.submitted);

        let reimbursement = document.createElement(`tr`);
        reimbursement.className = "table-tr"
        reimbursement.innerHTML = `<td class="table-td">${reimbursementRequest.id}</td>
        <td class="table-td">${reimbursementRequest.description}</td>
        <td class="table-td">$${reimbursementRequest.amount}</td>
        <td class="table-td">${date.toLocaleDateString()} ${date.toLocaleTimeString()}</td>
        <td class="table-td">${reimbursementRequest.type}</td>
        <td class="table-td">${reimbursementRequest.author}</td>`;

        reimbursement.innerHTML += `<td class="table-td">${reimbursementRequest.status}</td>`;
        reimbursementTBody.appendChild(reimbursement);
        
        reimbursementTHead.innerHTML = `<tr class="table-tr">
        <th>ID</th>
        <th>Description</th>
        <th>Amount</th>
        <th>Submitted</th>
        <th>Type</th>
        <th>Author</th>
        <th>Decision</th>
        </tr>`;
    });
}

