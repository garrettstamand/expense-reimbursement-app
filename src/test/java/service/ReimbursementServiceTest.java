package service;

import com.revature.dao.ReimbursementDaoImpl;
import com.revature.model.Reimbursement;
import com.revature.service.ReimbursementService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.sql.Blob;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ReimbursementServiceTest {


    ReimbursementDaoImpl reimbursementDaoMock = Mockito.mock(ReimbursementDaoImpl.class);


    @Test
    public void addReimbursement(){
        ReimbursementService reimbursementService = new ReimbursementService();
        reimbursementService.reimbursementDao = reimbursementDaoMock;
        Reimbursement reimbursement1 = new Reimbursement(1, 2.55f, null, null, "test", null, "bob", "sam", "Pending", "Lodging");

        Mockito.doNothing().when(reimbursementDaoMock).addReimbursement(reimbursement1, 1);

        reimbursementService.addReimbursement(reimbursement1, 1);

        Mockito.verify(reimbursementDaoMock, Mockito.times(1)).addReimbursement(reimbursement1, 1);

    }

    @Test
    public void getAllReimbursements(){
        ReimbursementService reimbursementService = new ReimbursementService();
        reimbursementService.reimbursementDao = reimbursementDaoMock;
        Reimbursement reimbursement1 = new Reimbursement(1, 2.55f, null, null, "test", null, "bob", "sam", "Pending", "Lodging");
        Reimbursement reimbursement2 = new Reimbursement(1, 2.55f, null, null, "test", null, "bob", "sam", "Pending", "Lodging");
        List<Reimbursement> reimbursements = new ArrayList<>();
        reimbursements.add(reimbursement1);
        reimbursements.add(reimbursement2);

        Mockito.when(reimbursementDaoMock.getReimbursements()).thenReturn(reimbursements);

        List<Reimbursement> returnedReimbursements = reimbursementService.getAllReimbursements();

        assertEquals(2, returnedReimbursements.size());
    }

    @Test
    public void approveReimbursementRequest(){

        ReimbursementService reimbursementService = new ReimbursementService();
        reimbursementService.reimbursementDao = reimbursementDaoMock;

        Mockito.doNothing().when(reimbursementDaoMock).approveReimbursement(1, true, 1);

        reimbursementService.approveReimbursementRequest(1, true, 1);

        Mockito.verify(reimbursementDaoMock, Mockito.times(1)).approveReimbursement(1, true, 1);

    }

}
