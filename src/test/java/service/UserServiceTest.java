package service;

import com.revature.dao.ReimbursementDaoImpl;
import com.revature.dao.UserDaoImpl;
import com.revature.model.User;
import com.revature.service.UserService;
import org.jasypt.util.password.StrongPasswordEncryptor;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class UserServiceTest {

    UserDaoImpl userDaoImplMock = Mockito.mock(UserDaoImpl.class);

    @Test
    public void attemptLogin(){

        StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();

        UserService userService = new UserService();
        userService.userDao = userDaoImplMock;

        User user = new User();
        user.setPassword(passwordEncryptor.encryptPassword("pw123"));
        user.setUsername("bob");

        Mockito.when(userDaoImplMock.getUser("bob")).thenReturn(user);

        userService.attemptLogin("bob", "pw123");

        assertTrue(passwordEncryptor.checkPassword("pw123", user.getPassword()));
        Mockito.verify(userDaoImplMock, Mockito.times(1)).getUser("bob");

    }

    @Test
    public void addUser(){
        UserService userService = new UserService();
        userService.userDao = userDaoImplMock;

        User user = new User();
        user.setPassword("pw123");
        user.setUsername("bob");

        Mockito.doNothing().when(userDaoImplMock).addUser(user);

        userService.addUser(user);

        Mockito.verify(userDaoImplMock, Mockito.times(1)).addUser(user);
    }

}
