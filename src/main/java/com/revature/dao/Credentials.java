package com.revature.dao;

public class Credentials {

    public static final String url = System.getenv("AWS_URL");
    public static String username = System.getenv("AWS_USERNAME");
    public static String password = System.getenv("AWS_PASSWORD");
}
