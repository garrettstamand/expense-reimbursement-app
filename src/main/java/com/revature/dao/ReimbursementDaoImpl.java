package com.revature.dao;

import com.revature.model.Reimbursement;
import com.revature.model.User;
import sun.plugin.javascript.navig.Image;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ReimbursementDaoImpl implements ReimbursementDao{

    public ReimbursementDaoImpl() {
        try{
            Class.forName("org.postgresql.Driver");
        }catch (ClassNotFoundException e){
            e.printStackTrace();
        }
    }


    @Override
    public List<Reimbursement> getReimbursements() {

        List<Reimbursement> reimbursements = new ArrayList<Reimbursement>();

        try(Connection conn = DriverManager.getConnection(Credentials.url, Credentials.username, Credentials.password)) {

            String sql = "select re.reimb_id, re.reimb_amount, re.reimb_submitted, re.reimb_resolved, re.reimb_description, re.reimb_receipt, us.user_first_name, us.user_last_name, eu.user_first_name, eu.user_last_name, ers.reimb_status, ert.reimb_type from ers_reimbursement re inner join ers_users us on re.reimb_author_fk = us.ers_users_id inner join ers_user_roles eur on eur.ers_user_role_id = us.user_role_id_fk inner join ers_reimbursement_status ers on ers.reimb_status_id = re.reimb_status_id_fk inner join ers_reimbursement_type ert on ert.reimb_type_id = re.reimb_type_id_fk left outer join ers_users eu on eu.ers_users_id = re.reimb_resolver_fk;";
            PreparedStatement ps = conn.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                reimbursements.add(processRS(rs));
            }

        }catch (SQLException e){
            e.printStackTrace();
        }
        return reimbursements;
    }

    @Override
    public List<Reimbursement> getReimbursements(User user) {
        List<Reimbursement> reimbursements = new ArrayList<Reimbursement>();

        try(Connection conn = DriverManager.getConnection(Credentials.url, Credentials.username, Credentials.password)) {

            String sql = "select re.reimb_id, re.reimb_amount, re.reimb_submitted, re.reimb_resolved, re.reimb_description, re.reimb_receipt, us.user_first_name, us.user_last_name, eu.user_first_name, eu.user_last_name, ers.reimb_status, ert.reimb_type from ers_reimbursement re inner join ers_users us on re.reimb_author_fk = us.ers_users_id inner join ers_user_roles eur on eur.ers_user_role_id = us.user_role_id_fk inner join ers_reimbursement_status ers on ers.reimb_status_id = re.reimb_status_id_fk inner join ers_reimbursement_type ert on ert.reimb_type_id = re.reimb_type_id_fk left outer join ers_users eu on eu.ers_users_id = re.reimb_resolver_fk where re.reimb_author_fk =?;";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, user.getId());
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                reimbursements.add(processRS(rs));
            }

        }catch (SQLException e){
            e.printStackTrace();
        }
        return reimbursements;
    }

    @Override
    public void approveReimbursement(int reimbursementID, boolean approved, int resolverID) {

        int status;
        if(approved) status = 2;
        else status = 3;

        try(Connection conn = DriverManager.getConnection(Credentials.url, Credentials.username, Credentials.password)){
            String sql = "update ers_reimbursement set reimb_status_id_fk = ?, reimb_resolver_fk = ?, reimb_resolved = ? where reimb_id = ?;";
            PreparedStatement ps = conn.prepareStatement(sql);

            java.util.Date DOB = new java.util.Date();
            Timestamp timestamp = new Timestamp(DOB.getTime());

            ps.setInt(1, status);
            ps.setInt(2, resolverID);
            ps.setTimestamp(3, timestamp);
            ps.setInt(4, reimbursementID);

            ps.executeUpdate();

        }catch (SQLException e){
            e.printStackTrace();
        }

    }

    @Override
    public void addReimbursement(Reimbursement reimbursement, int authorID) {
        java.util.Date DOB = new java.util.Date();
        Timestamp timestamp = new Timestamp(DOB.getTime());
        int type = 0;
        if(reimbursement.getType().equals("Lodging")) type = 1;
        if(reimbursement.getType().equals("Travel")) type = 2;
        if(reimbursement.getType().equals("Food")) type = 3;
        if(reimbursement.getType().equals("Other")) type = 4;

        try(Connection conn = DriverManager.getConnection(Credentials.url, Credentials.username, Credentials.password)){

            String sql = "insert into ers_reimbursement values(default, ?, ?, null, ?, ?, ?, null, ?, ?);";
            PreparedStatement ps = conn.prepareStatement(sql);

            ps.setFloat(1, reimbursement.getAmount());
            ps.setTimestamp(2, timestamp);
            ps.setString(3, reimbursement.getDescription());
            ps.setBlob(4, reimbursement.getReceipt());
            ps.setInt(5, authorID);
            ps.setInt(6, 1);
            ps.setInt(7, type);

            ps.executeUpdate();

        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    private Reimbursement processRS(ResultSet rs) throws SQLException {

        return new Reimbursement(rs.getInt(1),//reimbursement id
                rs.getFloat(2),//amount
                rs.getTimestamp(3),//submitted date
                rs.getTimestamp(4),//resolved date
                rs.getString(5),//description
                rs.getBlob(6),//image
                rs.getString(7) + " " + rs.getString(8),//author full name
                rs.getString(9) + " " + rs.getString(10),//resolver full name
                rs.getString(11),//status ie pending
                rs.getString(12)//type ie lodging
        );
    }

}
