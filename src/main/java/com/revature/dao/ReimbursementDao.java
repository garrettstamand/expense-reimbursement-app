package com.revature.dao;

import com.revature.model.Reimbursement;
import com.revature.model.User;

import java.util.List;

public interface ReimbursementDao {

    public List<Reimbursement> getReimbursements();
    public List<Reimbursement> getReimbursements(User user);
    public void approveReimbursement(int reimbursementID, boolean approved, int resolverID);
    public void addReimbursement(Reimbursement reimbursement, int authorID);

}
