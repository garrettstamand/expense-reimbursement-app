package com.revature.dao;

import com.revature.model.Reimbursement;
import com.revature.model.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl implements UserDao{

    public UserDaoImpl() {
        try{
            Class.forName("org.postgresql.Driver");
        }catch (ClassNotFoundException e){
            e.printStackTrace();
        }
    }

    @Override
    public User getUser(String username) {
        User user = null;

        try(Connection conn = DriverManager.getConnection(Credentials.url, Credentials.username, Credentials.password)) {

            String sql = "SELECT * FROM ers_users where ers_username = ?;";
            PreparedStatement ps = conn.prepareStatement(sql);

            ps.setString(1, username);

            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                user = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7));
            }

        }catch (SQLException e){
            e.printStackTrace();
        }
        return user;
    }
    @Override
    public User getUser(int userID) {
        User user = null;

        try(Connection conn = DriverManager.getConnection(Credentials.url, Credentials.username, Credentials.password)) {

            String sql = "SELECT * FROM ers_users where ers_user_id = ?;";
            PreparedStatement ps = conn.prepareStatement(sql);

            ps.setInt(1, userID);

            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                user = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7));
            }

        }catch (SQLException e){
            e.printStackTrace();
        }
        return user;
    }
    @Override
    public void addUser(User user) {

        try(Connection conn = DriverManager.getConnection(Credentials.url, Credentials.username, Credentials.password)){

            String sql = "insert into ers_users values(default, ?, ?, ?, ?, ?, ?);";
            PreparedStatement ps = conn.prepareStatement(sql);

            ps.setString(1, user.getUsername());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getFirstName());
            ps.setString(4, user.getLastName());
            ps.setString(5, user.getEmail());
            ps.setInt(6, user.getRoleID());

            ps.executeUpdate();

        }catch (SQLException e){
            e.printStackTrace();
        }
    }
}
