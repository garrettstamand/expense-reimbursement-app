package com.revature.dao;

import com.revature.model.User;

public interface UserDao {

    public User getUser(String username);
    public User getUser(int userID);
    public void addUser(User user);

}
