package com.revature.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.model.Response;
import com.revature.service.ReimbursementService;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.annotations.Test;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class GetAllReimbursementRequestsServletTest {

    HttpServletResponse responseMock = Mockito.mock(HttpServletResponse.class);
    PrintWriter printWriterMock = Mockito.mock(PrintWriter.class);
    ReimbursementService reimbursementServiceMock = Mockito.mock(ReimbursementService.class);

    @Test
    public void doGet() throws IOException, ServletException {

        GetAllReimbursementRequestsServlet getAllReimbursementRequestsServlet = new GetAllReimbursementRequestsServlet();
        getAllReimbursementRequestsServlet.setReimbursementService(reimbursementServiceMock);
        Mockito.when(responseMock.getWriter()).thenReturn(printWriterMock);
        Mockito.when(reimbursementServiceMock.getAllReimbursements()).thenReturn(null);
        Mockito.doNothing().when(printWriterMock).println(new ObjectMapper().writeValueAsString(null));
        getAllReimbursementRequestsServlet.doGet(null, responseMock);

        Mockito.verify(reimbursementServiceMock, Mockito.times(1)).getAllReimbursements();

    }

}
