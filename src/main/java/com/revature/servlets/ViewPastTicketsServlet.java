package com.revature.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.model.User;
import com.revature.service.ReimbursementService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.stream.Collectors;

@WebServlet(name = "ViewPastTickets", urlPatterns = "/api/ViewPastTickets")
public class ViewPastTicketsServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        ReimbursementService reimbursementService = new ReimbursementService();
        User user = (User) request.getSession().getAttribute("userObj");
        out.print(new ObjectMapper().writeValueAsString(reimbursementService.getAllReimbursements(user)));
    }
}