package com.revature.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.model.Reimbursement;
import com.revature.model.User;
import com.revature.service.ReimbursementService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

@WebServlet(name = "AddReimbursementRequest", urlPatterns = "/api/AddReimbursementRequest")
public class AddReimbursementRequestServlet extends HttpServlet {

    private ReimbursementService reimbursementService = new ReimbursementService();


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String requestBody = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));

        Reimbursement reimbursement = new ObjectMapper().readValue(requestBody,Reimbursement.class);

        HttpSession httpSession = request.getSession(true);
        User tempUser = (User) httpSession.getAttribute("userObj");

        reimbursementService.addReimbursement(reimbursement, tempUser.getId());
    }

    public void setReimbursementService(ReimbursementService reimbursementService) {
        this.reimbursementService = reimbursementService;
    }
}