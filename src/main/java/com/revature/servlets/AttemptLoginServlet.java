package com.revature.servlets;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.model.Response;
import com.revature.model.User;
import com.revature.service.UserService;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.stream.Collectors;

@WebServlet(name = "AttemptLogin", urlPatterns = "/api/AttemptLogin")
public class AttemptLoginServlet extends HttpServlet {

    private UserService userService = new UserService();


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        String requestBody = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
        User user = new ObjectMapper().readValue(requestBody,User.class);

        User tempUser = userService.attemptLogin(user.getUsername(), user.getPassword());
        if(tempUser != null){
            HttpSession httpSession = request.getSession(true);
            httpSession.setAttribute("userObj", tempUser);
            out.println(new ObjectMapper().writeValueAsString(new Response("Login successful", true, tempUser)));
        }else{
            out.println(new ObjectMapper().writeValueAsString(new Response("Invalid username or password", false, null)));
        }
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
