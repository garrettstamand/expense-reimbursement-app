package com.revature.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.service.ReimbursementService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Home", urlPatterns = "/api/home")
public class IndexServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        System.out.println("hit index");
        RequestDispatcher view = request.getRequestDispatcher("index.html");
        view.forward(request, response);
    }

}
