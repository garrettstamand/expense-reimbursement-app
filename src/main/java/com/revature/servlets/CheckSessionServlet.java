package com.revature.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.model.Response;
import com.revature.model.User;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "checkSession", urlPatterns = "/api/CheckSession")
public class CheckSessionServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        HttpSession httpSession = request.getSession(true);
        User tempUser = (User) httpSession.getAttribute("userObj");

        boolean isManager = false;
        if(tempUser != null) if(tempUser.getRoleID() == 1) isManager = true;

        out.println(new ObjectMapper().writeValueAsString(new Response("Manager", isManager, tempUser)));

    }

}
