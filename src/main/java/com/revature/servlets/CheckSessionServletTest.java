package com.revature.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.model.Response;
import com.revature.model.User;
import com.revature.service.UserService;
import org.mockito.Mockito;
import org.testng.annotations.Test;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

public class CheckSessionServletTest {

    HttpServletRequest requestMock = Mockito.mock(HttpServletRequest.class);
    HttpServletResponse responseMock = Mockito.mock(HttpServletResponse.class);
    PrintWriter printWriterMock = Mockito.mock(PrintWriter.class);

    @Test
    public void doGet() throws IOException, ServletException {

        User user = (User) getHttpSession().getAttribute("test");
        Mockito.when(responseMock.getWriter()).thenReturn(printWriterMock);
        Mockito.when(requestMock.getSession(true)).thenReturn(getHttpSession());
        Mockito.doNothing().when(printWriterMock).println(new Response("Manager", true, user));

        CheckSessionServlet checkSessionServlet = new CheckSessionServlet();
        checkSessionServlet.doGet(requestMock, responseMock);
        Response response = new Response("Manager", false, user);
        Mockito.verify(printWriterMock, Mockito.times(1)).println(new ObjectMapper().writeValueAsString(response));
    }





    private HttpSession getHttpSession(){
        return new HttpSession() {
            @Override
            public long getCreationTime() {
                return 0;
            }

            @Override
            public String getId() {
                return null;
            }

            @Override
            public long getLastAccessedTime() {
                return 0;
            }

            @Override
            public ServletContext getServletContext() {
                return null;
            }

            @Override
            public void setMaxInactiveInterval(int i) {

            }

            @Override
            public int getMaxInactiveInterval() {
                return 0;
            }

            @Override
            public HttpSessionContext getSessionContext() {
                return null;
            }

            @Override
            public Object getAttribute(String s) {
                User user = new User();
                user.setId(1);
                return user;
            }

            @Override
            public Object getValue(String s) {
                return null;
            }

            @Override
            public Enumeration<String> getAttributeNames() {
                return null;
            }

            @Override
            public String[] getValueNames() {
                return new String[0];
            }

            @Override
            public void setAttribute(String s, Object o) {

            }

            @Override
            public void putValue(String s, Object o) {

            }

            @Override
            public void removeAttribute(String s) {

            }

            @Override
            public void removeValue(String s) {

            }

            @Override
            public void invalidate() {

            }

            @Override
            public boolean isNew() {
                return false;
            }
        };
    }
}
