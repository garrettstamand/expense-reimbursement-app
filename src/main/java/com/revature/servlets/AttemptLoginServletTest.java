package com.revature.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.model.Response;
import com.revature.model.User;
import com.revature.service.UserService;
import org.mockito.Mockito;
import org.testng.annotations.Test;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.util.Enumeration;

public class AttemptLoginServletTest {

    HttpServletRequest requestMock = Mockito.mock(HttpServletRequest.class);
    HttpServletResponse responseMock = Mockito.mock(HttpServletResponse.class);
    UserService userServiceMock = Mockito.mock(UserService.class);
    PrintWriter printWriterMock = Mockito.mock(PrintWriter.class);

    @Test
    public void doPost() throws ServletException, IOException {

        String json = "{\n" +
                "    \"username\":\"sam123\",\n" +
                "    \"password\":\"pass123\",\n" +
                "    \"firstName\":\"Sam\",\n" +
                "    \"lastName\":\"Smith\",\n" +
                "    \"email\":\"test@test.com\",\n" +
                "    \"roleID\":1\n" +
                "}";

        AttemptLoginServlet attemptLoginServlet = new AttemptLoginServlet();
        attemptLoginServlet.setUserService(userServiceMock);
        Mockito.when(requestMock.getReader()).thenReturn(new BufferedReader(new StringReader(json)));
        Mockito.when(responseMock.getWriter()).thenReturn(printWriterMock);
        Mockito.when(requestMock.getSession(true)).thenReturn(getHttpSession());

        User user = new ObjectMapper().readValue(json,User.class);
        Mockito.when(userServiceMock.attemptLogin(user.getUsername(), user.getPassword())).thenReturn(user);

        Mockito.doNothing().when(printWriterMock).println(new ObjectMapper().writeValueAsString(new Response("Login successful", true, user)));

        attemptLoginServlet.doPost(requestMock, responseMock);
        Mockito.verify(userServiceMock, Mockito.times(1)).attemptLogin(user.getUsername(), user.getPassword());

    }
    private HttpSession getHttpSession() {
        return new HttpSession() {
            @Override
            public long getCreationTime() {
                return 0;
            }

            @Override
            public String getId() {
                return null;
            }

            @Override
            public long getLastAccessedTime() {
                return 0;
            }

            @Override
            public ServletContext getServletContext() {
                return null;
            }

            @Override
            public void setMaxInactiveInterval(int i) {

            }

            @Override
            public int getMaxInactiveInterval() {
                return 0;
            }

            @Override
            public HttpSessionContext getSessionContext() {
                return null;
            }

            @Override
            public Object getAttribute(String s) {
                User user = new User();
                user.setId(1);
                return user;
            }

            @Override
            public Object getValue(String s) {
                return null;
            }

            @Override
            public Enumeration<String> getAttributeNames() {
                return null;
            }

            @Override
            public String[] getValueNames() {
                return new String[0];
            }

            @Override
            public void setAttribute(String s, Object o) {

            }

            @Override
            public void putValue(String s, Object o) {

            }

            @Override
            public void removeAttribute(String s) {

            }

            @Override
            public void removeValue(String s) {

            }

            @Override
            public void invalidate() {

            }

            @Override
            public boolean isNew() {
                return false;
            }
        };
    }
}
