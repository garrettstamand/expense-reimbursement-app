package com.revature.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.model.Decision;
import com.revature.model.Reimbursement;
import com.revature.model.User;
import com.revature.service.ReimbursementService;
import org.mockito.Mockito;
import org.testng.annotations.Test;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.Enumeration;

public class ApproveReimbursementServletTest {

    HttpServletRequest requestMock = Mockito.mock(HttpServletRequest.class);
    ReimbursementService reimbursementServiceMock = Mockito.mock(ReimbursementService.class);

    @Test
    public void doPut() throws IOException, ServletException {

        String json = "{\n" +
                "    \"reimbursementID\":1,\n" +
                "    \"approved\":true\n" +
                "}";

        Mockito.when(requestMock.getReader()).thenReturn(new BufferedReader(new StringReader(json)));
        Mockito.when(requestMock.getSession()).thenReturn(getHttpSession());

        Decision decision =  new ObjectMapper().readValue((new BufferedReader(new StringReader(json))), Decision.class);
        User user = (User) getHttpSession().getAttribute("userObj");
        Mockito.doNothing().when(reimbursementServiceMock).approveReimbursementRequest(1, decision.isApproved(), user.getId());

        ApproveReimbursementServlet approveReimbursementServlet = new ApproveReimbursementServlet();
        approveReimbursementServlet.setReimbursementService(reimbursementServiceMock);
        approveReimbursementServlet.doPut(requestMock, null);
        Mockito.verify(reimbursementServiceMock, Mockito.times(1)).approveReimbursementRequest(1, decision.isApproved(), user.getId());

    }

    private HttpSession getHttpSession(){
        return new HttpSession() {
            @Override
            public long getCreationTime() {
                return 0;
            }

            @Override
            public String getId() {
                return null;
            }

            @Override
            public long getLastAccessedTime() {
                return 0;
            }

            @Override
            public ServletContext getServletContext() {
                return null;
            }

            @Override
            public void setMaxInactiveInterval(int i) {

            }

            @Override
            public int getMaxInactiveInterval() {
                return 0;
            }

            @Override
            public HttpSessionContext getSessionContext() {
                return null;
            }

            @Override
            public Object getAttribute(String s) {
                User user = new User();
                user.setId(1);
                return user;
            }

            @Override
            public Object getValue(String s) {
                return null;
            }

            @Override
            public Enumeration<String> getAttributeNames() {
                return null;
            }

            @Override
            public String[] getValueNames() {
                return new String[0];
            }

            @Override
            public void setAttribute(String s, Object o) {

            }

            @Override
            public void putValue(String s, Object o) {

            }

            @Override
            public void removeAttribute(String s) {

            }

            @Override
            public void removeValue(String s) {

            }

            @Override
            public void invalidate() {

            }

            @Override
            public boolean isNew() {
                return false;
            }
        };
    }

}
