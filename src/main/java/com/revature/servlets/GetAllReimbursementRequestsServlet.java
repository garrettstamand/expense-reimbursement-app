package com.revature.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.service.ReimbursementService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "GetAllReimbursementRequests", urlPatterns = "/api/GetAllReimbursementRequests")
public class GetAllReimbursementRequestsServlet extends HttpServlet {

    private ReimbursementService reimbursementService = new ReimbursementService();


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        out.print(new ObjectMapper().writeValueAsString(reimbursementService.getAllReimbursements()));
    }

    public void setReimbursementService(ReimbursementService reimbursementService) {
        this.reimbursementService = reimbursementService;
    }
}
