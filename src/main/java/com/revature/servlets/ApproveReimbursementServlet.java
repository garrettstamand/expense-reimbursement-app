package com.revature.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.model.Decision;
import com.revature.model.Reimbursement;
import com.revature.model.User;
import com.revature.service.ReimbursementService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.stream.Collectors;

@WebServlet(name = "ApproveReimbursement", urlPatterns = "/api/ApproveReimbursement")
public class ApproveReimbursementServlet extends HttpServlet {

    private ReimbursementService reimbursementService = new ReimbursementService();


    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String requestBody = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
        Decision decision = new ObjectMapper().readValue(requestBody,Decision.class);
        HttpSession httpSession = request.getSession();
        User user = (User) httpSession.getAttribute("userObj");
        reimbursementService.approveReimbursementRequest(decision.getReimbursementID(), decision.isApproved(), user.getId());
    }

    public void setReimbursementService(ReimbursementService reimbursementService) {
        this.reimbursementService = reimbursementService;
    }
}