package com.revature.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.model.Reimbursement;
import com.revature.model.User;
import com.revature.service.ReimbursementService;
import com.revature.servlets.AddReimbursementRequestServlet;
import org.mockito.Mock;
import org.testng.annotations.Test;
import org.mockito.Mockito;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.Enumeration;
import java.util.stream.Collectors;

public class AddReimbursementRequestServletTest {

    HttpServletRequest requestMock = Mockito.mock(HttpServletRequest.class);
    ReimbursementService reimbursementServiceMock = Mockito.mock(ReimbursementService.class);

    @Test
    public void doPost() throws IOException, ServletException {

        String json = "{\n" +
                "    \"id\":1,\n" +
                "    \"amount\":2.52,\n" +
                "    \"submitted\":\"2014-03-12T13:37:27+00:00\",\n" +
                "    \"resolved\":\"2014-03-13T13:37:27+00:00\",\n" +
                "    \"description\":\"test description\",\n" +
                "    \"receipt\":null,\n" +
                "    \"author\":\"test author\",\n" +
                "    \"resolver\":\"test resolver\",\n" +
                "    \"status\":\"test status\",\n" +
                "    \"type\":\"test type\"\n" +
                "}";
        Reimbursement reimbursement = new ObjectMapper().readValue((new BufferedReader(new StringReader(json))),Reimbursement.class);

        System.out.println(reimbursement);
        Mockito.when(requestMock.getReader()).thenReturn(new BufferedReader(new StringReader(json)));
        Mockito.when(requestMock.getSession(true)).thenReturn(getHttpSession());
        Mockito.doNothing().when(reimbursementServiceMock).addReimbursement(reimbursement, 1);

        AddReimbursementRequestServlet addReimbursementRequestServlet = new AddReimbursementRequestServlet();
        addReimbursementRequestServlet.setReimbursementService(reimbursementServiceMock);
        addReimbursementRequestServlet.doPost(requestMock, null);

        Mockito.verify(reimbursementServiceMock, Mockito.times(1)).addReimbursement(reimbursement, 1);

    }

    private HttpSession getHttpSession(){
        return new HttpSession() {
            @Override
            public long getCreationTime() {
                return 0;
            }

            @Override
            public String getId() {
                return null;
            }

            @Override
            public long getLastAccessedTime() {
                return 0;
            }

            @Override
            public ServletContext getServletContext() {
                return null;
            }

            @Override
            public void setMaxInactiveInterval(int i) {

            }

            @Override
            public int getMaxInactiveInterval() {
                return 0;
            }

            @Override
            public HttpSessionContext getSessionContext() {
                return null;
            }

            @Override
            public Object getAttribute(String s) {
                User user = new User();
                user.setId(1);
                return user;
            }

            @Override
            public Object getValue(String s) {
                return null;
            }

            @Override
            public Enumeration<String> getAttributeNames() {
                return null;
            }

            @Override
            public String[] getValueNames() {
                return new String[0];
            }

            @Override
            public void setAttribute(String s, Object o) {

            }

            @Override
            public void putValue(String s, Object o) {

            }

            @Override
            public void removeAttribute(String s) {

            }

            @Override
            public void removeValue(String s) {

            }

            @Override
            public void invalidate() {

            }

            @Override
            public boolean isNew() {
                return false;
            }
        };
    }
}

