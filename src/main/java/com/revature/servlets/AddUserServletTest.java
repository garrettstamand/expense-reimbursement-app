package com.revature.servlets;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.model.User;
import com.revature.service.ReimbursementService;
import com.revature.service.UserService;
import org.mockito.Mockito;
import org.testng.annotations.Test;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;

public class AddUserServletTest {

    HttpServletRequest requestMock = Mockito.mock(HttpServletRequest.class);
    UserService userServiceMock = Mockito.mock(UserService.class);    private int id;

    @Test
    public void doPost() throws IOException, ServletException {

        String json = "{\n" +
                "    \"username\":\"sam123\",\n" +
                "    \"password\":\"pass123\",\n" +
                "    \"firstName\":\"Sam\",\n" +
                "    \"lastName\":\"Smith\",\n" +
                "    \"email\":\"test@test.com\",\n" +
                "    \"roleID\":1\n" +
                "}";

        User user = new ObjectMapper().readValue(json, User.class);
        AddUserServlet addUserServlet = new AddUserServlet();
        addUserServlet.setUserService(userServiceMock);
        Mockito.when(requestMock.getReader()).thenReturn(new BufferedReader(new StringReader(json)));
        Mockito.doNothing().when(userServiceMock).addUser(user);
        addUserServlet.doPost(requestMock, null);

        Mockito.verify(userServiceMock, Mockito.times(1)).addUser(user);

    }

}
