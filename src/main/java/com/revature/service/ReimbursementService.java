package com.revature.service;

import com.revature.dao.ReimbursementDaoImpl;
import com.revature.model.Reimbursement;
import com.revature.model.User;

import java.util.List;

public class ReimbursementService {

    public ReimbursementDaoImpl reimbursementDao = new ReimbursementDaoImpl();

    public void addReimbursement(Reimbursement reimbursement, int authorID){
        reimbursementDao.addReimbursement(reimbursement, authorID);
    }

    public List<Reimbursement> getAllReimbursements(){
        return reimbursementDao.getReimbursements();
    }

    public List<Reimbursement> getAllReimbursements(User user){
        return reimbursementDao.getReimbursements(user);
    }

    public void approveReimbursementRequest(int reimbursementID, boolean approved, int resolverID){
        reimbursementDao.approveReimbursement(reimbursementID, approved, resolverID);
    }

}
