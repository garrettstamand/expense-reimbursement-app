package com.revature.service;

import com.revature.dao.UserDaoImpl;
import com.revature.model.User;
import org.jasypt.util.password.StrongPasswordEncryptor;

public class UserService {

    public UserDaoImpl userDao = new UserDaoImpl();

    public User attemptLogin(String username, String password){
        StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
        User user = userDao.getUser(username);
        if(user == null) return null;
        if(passwordEncryptor.checkPassword(password, user.getPassword())) return user;
        return null;
    }

    public void addUser(User user){
        StrongPasswordEncryptor passwordEncryptor = new StrongPasswordEncryptor();
        String encryptedPassword = passwordEncryptor.encryptPassword(user.getPassword());
        user.setPassword(encryptedPassword);
        userDao.addUser(user);
    }

}
