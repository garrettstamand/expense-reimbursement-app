package com.revature.model;

import java.util.Objects;

public class Decision {

    private int reimbursementID;
    private boolean approved;

    public Decision(){}

    public int getReimbursementID() {
        return reimbursementID;
    }

    public void setReimbursementID(int reimbursementID) {
        this.reimbursementID = reimbursementID;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    @Override
    public String toString() {
        return "Decision{" +
                "reimbursementID=" + reimbursementID +
                ", approved=" + approved +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Decision decision = (Decision) o;
        return reimbursementID == decision.reimbursementID && approved == decision.approved;
    }

    @Override
    public int hashCode() {
        return Objects.hash(reimbursementID, approved);
    }
}
