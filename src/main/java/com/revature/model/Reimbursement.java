package com.revature.model;

import com.revature.service.ReimbursementService;
import sun.plugin.javascript.navig.Image;

import java.sql.Blob;
import java.sql.Timestamp;

public class Reimbursement {

    private int id;
    private float amount;
    private Timestamp submitted;
    private Timestamp resolved;
    private String description;

    private Blob receipt;
    private String author;
    private String resolver;
    private String status;
    private String type;

    public Reimbursement(){};

    public Reimbursement(int id, float amount, Timestamp submitted, Timestamp resolved, String description, Blob receipt, String author, String resolver, String status, String type) {
        this.id = id;
        this.amount = amount;
        this.submitted = submitted;
        this.resolved = resolved;
        this.description = description;
        this.receipt = receipt;
        this.author = author;
        this.resolver = resolver;
        this.status = status;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public Timestamp getSubmitted() {
        return submitted;
    }

    public void setSubmitted(Timestamp submitted) {
        this.submitted = submitted;
    }

    public Timestamp getResolved() {
        return resolved;
    }

    public void setResolved(Timestamp resolved) {
        this.resolved = resolved;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Blob getReceipt() {
        return receipt;
    }

    public void setReceipt(Blob receipt) {
        this.receipt = receipt;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getResolver() {
        return resolver;
    }

    public void setResolver(String resolver) {
        this.resolver = resolver;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Reimbursement{" +
                "ID=" + id +
                ", amount=" + amount +
                ", submitted=" + submitted +
                ", resolved=" + resolved +
                ", description='" + description + '\'' +
                ", receipt=" + receipt +
                ", author=" + author +
                ", resolver=" + resolver +
                ", status=" + status +
                ", type=" + type +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        Reimbursement other = (Reimbursement) obj;
        if(this.id != other.id) return false;
        if(!this.submitted.equals(other.submitted)) return false;
        if(!this.resolved.equals(other.resolved)) return false;
        if(!this.description.equals(other.description)) return false;
        if(!this.author.equals(other.author)) return false;
        if(!this.resolver.equals(other.resolver)) return false;
        if(!this.status.equals(other.status)) return false;
        if(!this.type.equals(other.type)) return false;
        return true;
    }
}
